import tfvars 

tfv = tfvars.LoadSecrets()

print(dir(tfv))

print(tfv.keys())

print(tfv["token"])

print(len(tfv))

print(type(tfv))

for k, v in tfv.items():
    print(f"{k} = {v}")
